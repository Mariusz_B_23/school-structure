import java.util.Objects;

public class Klasa {

    private int nrSali;
    private int ilośćKrzeseł;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Klasa klasa = (Klasa) o;
        return nrSali == klasa.nrSali && ilośćKrzeseł == klasa.ilośćKrzeseł;
    }

    @Override
    public int hashCode() {
        return Objects.hash(nrSali, ilośćKrzeseł);
    }

    @Override
    public String toString() {
        return "Klasa{" +
                "nrSali=" + nrSali +
                ", ilośćKrzeseł=" + ilośćKrzeseł +
                '}';
    }

}
