import osoby.Nauczyciel;
import osoby.Uczeń;

import java.util.List;
import java.util.Objects;

public class GrupaUczniów {

    private int idGrupy;
    private Nauczyciel wychowawca;
    private List<Uczeń> uczniowie;
    private Klasa przypisanaKlasa;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GrupaUczniów that = (GrupaUczniów) o;
        return idGrupy == that.idGrupy && Objects.equals(wychowawca, that.wychowawca) && Objects.equals(uczniowie, that.uczniowie) && Objects.equals(przypisanaKlasa, that.przypisanaKlasa);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idGrupy, wychowawca, uczniowie, przypisanaKlasa);
    }

    @Override
    public String toString() {
        return "GrupaUczniów{" +
                "idGrupy=" + idGrupy +
                ", wychowawca=" + wychowawca +
                ", uczniowie=" + uczniowie +
                ", przypisanaKlasa=" + przypisanaKlasa +
                '}';
    }
}
