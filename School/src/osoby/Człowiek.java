package osoby;

import java.util.Objects;

public class Człowiek {

    private String imię;
    private String nazwisko;
    private int wiek;
    private Płeć płeć;
    private String pesel;
    private Address adres;


    public void niePrzeklinaj(){

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Człowiek człowiek = (Człowiek) o;
        return wiek == człowiek.wiek && Objects.equals(imię, człowiek.imię) && Objects.equals(nazwisko, człowiek.nazwisko) && płeć == człowiek.płeć && Objects.equals(pesel, człowiek.pesel) && Objects.equals(adres, człowiek.adres);
    }

    @Override
    public int hashCode() {
        return Objects.hash(imię, nazwisko, wiek, płeć, pesel, adres);
    }

    @Override
    public String toString() {
        return "Człowiek{" +
                "imię='" + imię + '\'' +
                ", nazwisko='" + nazwisko + '\'' +
                ", wiek=" + wiek +
                ", płeć=" + płeć +
                ", pesel='" + pesel + '\'' +
                ", adres=" + adres +
                '}';
    }
}
