package osoby;

import java.util.Objects;

public class Nauczyciel extends PracownikSzkoły{

    private String wykładanyPrzedmiot;

    @Override
    public void przyjdźDoPracy() {
        super.przyjdźDoPracy();
    }

    @Override
    public void wyjdźZPracy() {
        super.wyjdźZPracy();
    }

    @Override
    public void niePrzeklinaj() {
        super.niePrzeklinaj();
    }

    public void uczSwojegoPrzedmiotu (String wykładanyPrzedmiot) {

    }

    public void wypełnijDziennik() {

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Nauczyciel that = (Nauczyciel) o;
        return Objects.equals(wykładanyPrzedmiot, that.wykładanyPrzedmiot);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), wykładanyPrzedmiot);
    }

    @Override
    public String toString() {
        return "Nauczyciel{" +
                "wykładanyPrzedmiot='" + wykładanyPrzedmiot + '\'' +
                '}';
    }

}
