package osoby;

import java.util.List;
import java.util.Objects;

public class Uczeń extends Człowiek {

    private String imięMatki;
    private String imięOjca;
    private List<String> oceny;



    @Override
    public void niePrzeklinaj() {
        super.niePrzeklinaj();
    }

    public void idźDoSzkoły() {

    }

    public void odróbZadanieDomowe() {

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Uczeń uczeń = (Uczeń) o;
        return Objects.equals(imięMatki, uczeń.imięMatki) && Objects.equals(imięOjca, uczeń.imięOjca) && Objects.equals(oceny, uczeń.oceny);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), imięMatki, imięOjca, oceny);
    }

    @Override
    public String toString() {
        return "Uczeń{" +
                "imięMatki='" + imięMatki + '\'' +
                ", imięOjca='" + imięOjca + '\'' +
                ", oceny=" + oceny +
                '}';
    }

}
