package osoby;

import java.math.BigDecimal;
import java.util.Objects;

public class PracownikSzkoły extends Człowiek {

    private String szkolneId;
    private BigDecimal pensja;
    private Wykształcenie wykształcenie;

    @Override
    public void niePrzeklinaj() {
        super.niePrzeklinaj();
    }

    public void przyjdźDoPracy() {

    }

    public void wyjdźZPracy() {

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        PracownikSzkoły that = (PracownikSzkoły) o;
        return Objects.equals(szkolneId, that.szkolneId) && Objects.equals(pensja, that.pensja) && wykształcenie == that.wykształcenie;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), szkolneId, pensja, wykształcenie);
    }

    @Override
    public String toString() {
        return "PracownikSzkoły{" +
                "szkolneId='" + szkolneId + '\'' +
                ", pensja=" + pensja +
                ", wykształcenie=" + wykształcenie +
                '}';
    }
}
