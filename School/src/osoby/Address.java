package osoby;

import java.util.Objects;

public class Address {

    private  String ulica;
    private  String numer;
    private  String miasto;
    private  String kodPocztowy;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Address address = (Address) o;
        return Objects.equals(ulica, address.ulica) && Objects.equals(numer, address.numer) && Objects.equals(miasto, address.miasto) && Objects.equals(kodPocztowy, address.kodPocztowy);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ulica, numer, miasto, kodPocztowy);
    }

    @Override
    public String toString() {
        return "Address{" +
                "ulica='" + ulica + '\'' +
                ", numer='" + numer + '\'' +
                ", miasto='" + miasto + '\'' +
                ", kodPocztowy='" + kodPocztowy + '\'' +
                '}';
    }
}
