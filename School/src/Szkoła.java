import osoby.Address;

import java.util.Objects;

public class Szkoła {

    private String nazwaSzkoły;
    private Address address;
    private int ilośćPracowników;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Szkoła szkoła = (Szkoła) o;
        return ilośćPracowników == szkoła.ilośćPracowników && Objects.equals(nazwaSzkoły, szkoła.nazwaSzkoły) && Objects.equals(address, szkoła.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nazwaSzkoły, address, ilośćPracowników);
    }

    @Override
    public String toString() {
        return "Szkoła{" +
                "nazwaSzkoły='" + nazwaSzkoły + '\'' +
                ", address=" + address +
                ", ilośćPracowników=" + ilośćPracowników +
                '}';
    }

}
